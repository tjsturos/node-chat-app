var socket = io();

function scrollToBottom () {
  // Selectors
var messages = $("#messages");
var newMessage = messages.children('li:last-child');
  //Heights
  var clientHeight = messages.prop('clientHeight');
  var scrollTop = messages.prop('scrollTop');
  var scrollHeight = messages.prop('scrollHeight');
  var newMessageHeight = $(newMessage).innerHeight();
  var lastMessageHeight = $(newMessage).prev("li").innerHeight();

  // console.log(clientHeight + " + " + scrollTop + " + " + newMessageHeight + " + " + lastMessageHeight + " vs. " + scrollHeight);
  if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
    messages.scrollTop(scrollHeight);
  }

}


socket.on('connect', function () {
  var params = $.deparam(window.location.search);
  console.log("connected")
  socket.emit('join', params, function (err) {
    console.log('callback for join');
    if (err) {
      alert(err);
      window.location.href = '/';
    } else {
      console.log("No error!");
    }
  });
});

socket.on('disconnect', function () {
  console.log('Disconnected from server');
});

socket.on('updateUserList', function (users) {
  var ol = $('<ol></ol>');

  users.forEach(function (user) {
    ol.append($('<li></li>').text(user));
  });

  $("#users").html(ol);
});

socket.on('newMessage', function (message) {
 var formattedTime = moment(message.createdAt).format('h:mm a');
  var template = $("#message-template").html();
  var html = Mustache.render(template, {
    text: message.text,
    from: message.from,
    createdAt: formattedTime
  });

  $("#messages").append(html);
  scrollToBottom();
});

socket.on('newLocationMessage', function (message) {
  var formattedTime = moment(message.createdAt).format('h:mm a');
  var template = $("#location-message-template").html();
  var html = Mustache.render(template, {
    url: message.url,
    from: message.from,
    createdAt: formattedTime
  });
  $('#messages').append(html);
  scrollToBottom();
});

$('#message-form').on('submit', function (e) {
  e.preventDefault();

  var messageTextBox = $('[name=message]');
  socket.emit('createMessage', {
    text: messageTextBox.val()
  }, function (err) {
    if(err) {
      messageTextBox.val(err);
      //console.log(err);
    }
      messageTextBox.val("")
  });
});

var locationButton = $('#send-location');
locationButton.on('click', function () {
  if (!navigator.geolocation) {
    return alert('Geolocation not supported by your browser.');
  }
  locationButton.attr('disabled', true).html('Sending...');

  navigator.geolocation.getCurrentPosition(function (position) {
    locationButton.removeAttr('disabled').html('Send Location');
    socket.emit('createLocationMessage', {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    });
  }, function () {
    locationButton.removeAttr('disabled').html('Send Location');
    alert('Unable to fetch location.');
  });
});
