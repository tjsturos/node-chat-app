const expect = require('expect');

const {isRealString} = require('./validation');

describe('isRealString', () => {
  it('should reject non-string values', () => {
    var isString = isRealString(1234);

    expect(isString).toBe(false);

  });

  it('should reject strings with only spaces', () => {
    var isString = isRealString("     ");

    expect(isString).toBe(false);

  });

  it('should accept strings with non-space characters', () => {
    var isString = isRealString("  a   ");

    expect(isString).toBe(true);

  });
});
