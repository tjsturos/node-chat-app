const expect = require('expect');

const {Users} = require('./users');

describe('Users', () => {
  var users;

  beforeEach(() => {
    users = new Users();

    users.users = [{
      id : '1',
      name : "John",
      room : "Node Course"
    },{
      id : '2',
      name : "Abe",
      room : "Real Men"
    },{
      id : '3',
      name : "Sam",
      room : "Node Course"
    }];
  });

  it('should add a new user', () => {
    var users = new Users();
    var user = {
      id: '123',
      name: "Tyler",
      room: "Cool Guy Room"
    };

    var resUser = users.addUser(user.id, user.name, user.room);

    expect(users.users).toEqual([user]);

  });

  it('should return names for Node Course', () => {
    var userList = users.getUserList('Node Course');

    expect(userList).toEqual(["John", "Sam"]);
  });

  it('should return names for Real Men', () => {
    var userList = users.getUserList('Real Men');

    expect(userList).toEqual(["Abe"]);
  });

  it('should remove a user', () => {
    var user = users.removeUser("1");

    expect(user).toEqual({
      id: "1",
      name: "John",
      room: "Node Course"
    });

    expect(users.users.length).toBe(2);
  });

  it('should not remove a user', () => {
    var user = users.removeUser('4');

    expect(user).toBe(null);
    expect(users.users.length).toBe(3);

  })

  it('should return an existing user', () => {
    var user = users.getUser("3");

    expect(user.id).toEqual("3");
  });

  it('should not find non-existing user', () => {
    var user = users.getUser('     ');

    expect(user).toBe(null);
  })


});
