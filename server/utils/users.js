

//addUser(id,name,room)
//removeUser(id)
//getUser(id)
//getUserList(room)

class Users {
  constructor () {
    this.users = [];
  }

  addUser (id, name, room) {
    var user = {id, name, room};
    this.users.push(user);
    return user;
  }

  removeUser (id) {
    var users = this.users;
    for(var i = 0; i < users.length; i++) {
    var obj = users[i];

      if(id.indexOf(obj.id) > -1) {
        users.splice(i, 1);
        return obj;
      }
    }

    return null;
  }

  getUser (id) {
        var user = this.users.filter((user) => user.id === id)[0];
        if(!user) return null;
        return user;
  }

  getUserList (room) {
    var users = this.users.filter((user) => user.room === room);
    var namesArray = users.map((user) => user.name);
    return namesArray;
  }
}

module.exports = {Users};
