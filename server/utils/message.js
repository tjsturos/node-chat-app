var moment = require('moment');

var currentTime = moment().valueOf();

var generateMessage = (from, text) => {
  return {
    from,
    text,
    createdAt: currentTime
  }
};

var generateLocationMessage = (from, latitude, longitude) => {
  return {
    from,
    url: `https://www.google.com/maps?=${latitude},${longitude}`,
    createdAt: currentTime
  }
}

module.exports = {generateMessage, generateLocationMessage};
