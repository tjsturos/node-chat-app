var expect = require('expect');

var {generateMessage, generateLocationMessage} = require('./message');

describe('generateMessage', () => {
  it('should generate a proper message', () => {
    var from = "TestUser";
    var text = "It is lovely out today!";
    var message = generateMessage(from, text);

    expect(message).toInclude({ from, text });

    expect(message.createdAt).toBeA('number');

  });
});

describe('generateLocationMessage', () => {
  it('should generate correction location object', () => {
      var from = "TestAdmin";
      var latitude = "45.2344668"
      var longitude = '-93.5684651';
      var locationMessage = generateLocationMessage(from, latitude, longitude);

      expect(locationMessage.createdAt).toBeA('number');

      expect(locationMessage).toInclude({
        from,
        url: `https://www.google.com/maps?=${latitude},${longitude}`
      });

  })
})
